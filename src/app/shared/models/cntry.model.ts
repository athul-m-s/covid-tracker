import { country } from './country.model';

export interface cntry {
  country: string;
  cases: number;
  deaths: number;
  recovered: number;
  tests: number;
  population: number;
  countryInfo: country;
}
