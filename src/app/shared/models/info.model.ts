export interface DashboardInfo {
  totalCases: number;
  totalDeaths: number;
  totalRecovered: number;
  todayCases: number;
  todayDeaths: number;
  todayRecovered: number;
}
