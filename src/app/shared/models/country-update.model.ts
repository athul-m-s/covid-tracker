export interface CountryUpdate {
  name: string;
  cases: number;
  deaths: number;
  recovered: number;
  tests: number;
}
