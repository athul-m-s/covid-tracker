import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxPaginationModule } from 'ngx-pagination';
import { NoDataComponent } from './component/no-data/no-data.component';
import { ToggleDropdownDirective } from './directives/toggle-dropdown.directive';
import { MillionPipe } from './pipe/million.pipe';

@NgModule({
  declarations: [MillionPipe, ToggleDropdownDirective, NoDataComponent],
  imports: [
    CommonModule,
    NgxPaginationModule,
    FontAwesomeModule,
    ReactiveFormsModule,
  ],
  exports: [
    MillionPipe,
    ToggleDropdownDirective,
    NoDataComponent,
    NgxPaginationModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    CommonModule,
  ],
})
export class SharedModule {}
