import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryElementComponent } from './country-element.component';

describe('CountryElementComponent', () => {
  let component: CountryElementComponent;
  let fixture: ComponentFixture<CountryElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryElementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
