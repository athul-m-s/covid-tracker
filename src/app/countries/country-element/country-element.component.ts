import { Component, Input, OnInit } from '@angular/core';
import { cntry } from 'src/app/shared/models/cntry.model';

@Component({
  selector: 'app-country-element',
  templateUrl: './country-element.component.html',
  styleUrls: ['./country-element.component.css'],
})
export class CountryElementComponent implements OnInit {
  @Input() cntry: cntry;
  constructor() {}

  ngOnInit(): void {}
}
