import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataStorageService } from '../services/data-storage.service';
import { CountryData } from '../shared/models/country-info.model';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css'],
})
export class CountriesComponent implements OnInit, OnDestroy {
  public countryData: CountryData[] = [];
  public filteredCountry: CountryData[] = [];

  public cntry_sub: Subscription;

  public filterText: string = 'Sort by';
  public p: number = 1;
  public itemsPerPage: number = 30;

  constructor(private dataStorageService: DataStorageService) {}

  ngOnInit(): void {
    this.setValues();
  }

  setValues() {
    let data = this.dataStorageService.CountryData;
    if (!data) {
      this.cntry_sub = this.dataStorageService
        .fetchCountryData()
        .subscribe((res) => {
          this.countryData = this.filteredCountry = res;
        });
    } else {
      this.countryData = this.filteredCountry = data;
    }
  }

  filter(value: string) {
    this.p = 1;
    this.filteredCountry = this.countryData;
    this.filteredCountry = value
      ? this.countryData.filter((c) =>
          c.country.toLowerCase().includes(value.toLowerCase())
        )
      : this.countryData;
  }

  onSort(event) {
    this.filterText = event.target.innerText;
    switch (this.filterText) {
      case 'Country name':
        this.filteredCountry = this.executeSort('country');
        break;
      case 'Number of cases':
        this.filteredCountry = this.executeSort('cases');
        break;
      case 'Number of deaths':
        this.filteredCountry = this.executeSort('deaths');
        break;
      case 'Number of recovered':
        this.filteredCountry = this.executeSort('recovered');
        break;
      default:
        this.filteredCountry = this.executeSort('country');
        break;
    }
  }

  executeSort(field: string) {
    return this.filteredCountry.sort((a, b) => {
      if (a[field] < b[field]) {
        return -1;
      } else {
        return a[field] > b[field] ? 1 : 0;
      }
    });
  }

  ngOnDestroy() {
    this.cntry_sub ? this.cntry_sub.unsubscribe : null;
  }
}
