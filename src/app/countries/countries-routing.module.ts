import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../auth/auth-guard.service';
import { CountriesEditComponent } from './countries-edit/countries-edit.component';
import { CountriesComponent } from './countries.component';

const routes: Routes = [
  {
    path: '',
    component: CountriesComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: ':name',
    component: CountriesEditComponent,
    canActivate: [AuthGuardService],
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CountryRoutingModule {}
