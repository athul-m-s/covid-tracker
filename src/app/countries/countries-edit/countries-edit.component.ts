import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataStorageService } from 'src/app/services/data-storage.service';
import { CountryUpdate } from 'src/app/shared/models/country-update.model';

@Component({
  selector: 'app-countries-edit',
  templateUrl: './countries-edit.component.html',
  styleUrls: ['./countries-edit.component.css'],
})
export class CountriesEditComponent implements OnInit, OnDestroy {
  public countryName: string = '';
  public countryFormGroup: FormGroup;
  public ds_sub: Subscription;
  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private dataStorageService: DataStorageService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.countryName = this.route.snapshot.params['name'];
    this.createCountryFormGroup();

    if (!this.dataStorageService.CountryData) {
      this.ds_sub = this.dataStorageService
        .fetchCountryData()
        .subscribe((res) => res);
    }
  }

  createCountryFormGroup() {
    this.countryFormGroup = this.fb.group({
      cases: [
        '',
        [
          Validators.required,
          Validators.min(0),
          Validators.pattern('^[0-9]*$'),
        ],
      ],
      deaths: [
        '',
        [
          Validators.required,
          Validators.min(0),
          Validators.pattern('^[0-9]*$'),
        ],
      ],
      recovered: [
        '',
        [
          Validators.required,
          Validators.min(0),
          Validators.pattern('^[0-9]*$'),
        ],
      ],
      tests: [
        '',
        [
          Validators.required,
          Validators.min(0),
          Validators.pattern('^[0-9]*$'),
        ],
      ],
    });
  }

  onSubmit() {
    if (this.countryFormGroup.invalid) {
      return;
    } else {
      this.updateData();
    }
  }

  updateData() {
    let data = {} as CountryUpdate;
    data.name = this.countryName;
    data.cases = this.countryFormGroup.get('cases').value;
    data.deaths = this.countryFormGroup.get('deaths').value;
    data.recovered = this.countryFormGroup.get('recovered').value;
    data.tests = this.countryFormGroup.get('tests').value;
    this.dataStorageService.UpdateCountryData(data);
  }

  onCancel() {
    this.countryFormGroup.reset();
    this.router.navigate(['/countries']);
  }

  ngOnDestroy() {
    this.ds_sub ? this.ds_sub.unsubscribe() : null;
  }
}
