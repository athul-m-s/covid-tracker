import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountriesComponent } from './countries.component';
import { CountriesEditComponent } from './countries-edit/countries-edit.component';
import { CountryElementComponent } from './country-element/country-element.component';
import { CountryRoutingModule } from './countries-routing.module';
import { SharedModule } from '../shared/shared-module';

@NgModule({
  declarations: [
    CountriesComponent,
    CountriesEditComponent,
    CountryElementComponent,
  ],
  imports: [CountryRoutingModule, SharedModule],
  exports: [],
})
export class CountrydModule {}
