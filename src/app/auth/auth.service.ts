import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public isLoggedIn = new BehaviorSubject<boolean>(false);

  constructor(private router: Router) {}

  login(username: string, password: string) {
    let credential = 'fingent';
    if (username.trim() == credential && password.trim() == credential) {
      localStorage.setItem('isLoggedIn', JSON.stringify('true'));
      this.isLoggedIn.next(true);
      return of(true);
    }
    return of(false);
  }

  logout() {
    this.isLoggedIn.next(false);
    this.router.navigate(['/auth']);
    localStorage.removeItem('isLoggedIn');
  }

  autoLogin() {
    const isLoggedIn = JSON.parse(localStorage.getItem('isLoggedIn'));
    if (!isLoggedIn) {
      return;
    }

    this.isLoggedIn.next(true);
  }
}
