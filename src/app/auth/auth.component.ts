import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
})
export class AuthComponent implements OnInit {
  public error: boolean = false;
  public authForm: FormGroup;

  private auth_sub: Subscription;

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.createAuthForm();
  }

  createAuthForm() {
    this.authForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  onSubmit() {
    if (!this.authForm) {
      return;
    }

    const username = this.authForm.get('username').value;
    const password = this.authForm.get('password').value;

    this.auth_sub = this.authService.login(username, password).subscribe(
      (res) => {
        if (res) {
          this.error = false;
          this.router.navigate(['/dashboard']);
        } else {
          this.error = true;
        }
      },
      (err) => {
        this.error = true;
      }
    );

    this.authForm.reset();
  }

  ngOnDestroy() {
    this.auth_sub ? this.auth_sub.unsubscribe : null;
  }
}
