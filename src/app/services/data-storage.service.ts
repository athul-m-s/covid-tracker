import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CovidInfo } from '../shared/models/covid-info.model';
import { CountryData } from '../shared/models/country-info.model';
import { CountryUpdate } from '../shared/models/country-update.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class DataStorageService {
  public CovidInfo: CovidInfo;
  public CountryData: CountryData[];

  constructor(private http: HttpClient, private router: Router) {}

  fetchData() {
    return this.http.get<CovidInfo>(environment.api).pipe(
      map((res) => {
        this.CovidInfo = res;
        return res;
      })
    );
  }

  fetchCountryData() {
    return this.http.get<CountryData[]>(environment.countryApi).pipe(
      map((res) => {
        this.CountryData = res;
        return res;
      })
    );
  }

  UpdateCountryData(data: CountryUpdate) {
    this.CountryData.forEach((cntry) => {
      if (cntry.country == data.name) {
        cntry.cases = data.cases;
        cntry.deaths = data.deaths;
        cntry.recovered = data.recovered;
        cntry.tests = data.tests;
        this.router.navigate(['/countries']);
      }
    });
  }
}
