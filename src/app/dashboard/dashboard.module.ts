import { NgModule } from '@angular/core';
import { DashboardElementComponent } from './dashboard-element/dashboard-element.component';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../shared/shared-module';

@NgModule({
  declarations: [DashboardComponent, DashboardElementComponent],
  imports: [DashboardRoutingModule, SharedModule],
  exports: [],
})
export class DashboardModule {}
