import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataStorageService } from '../services/data-storage.service';
import { CovidInfo } from '../shared/models/covid-info.model';
import { DashboardInfo } from '../shared/models/info.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  public info: DashboardInfo;
  public ds_sub: Subscription;
  constructor(private dataStorageService: DataStorageService) {}

  ngOnInit(): void {
    this.setValues();
  }

  setValues() {
    let data = this.dataStorageService.CovidInfo;
    if (!data) {
      this.ds_sub = this.dataStorageService.fetchData().subscribe((res) => {
        this.info = this.setData(res);
      });
    } else {
      this.info = this.setData(data);
    }
  }

  setData(val: CovidInfo) {
    let dashboardinfo = {} as DashboardInfo;
    dashboardinfo.totalCases = val.cases;
    dashboardinfo.totalDeaths = val.deaths;
    dashboardinfo.totalRecovered = val.recovered;
    dashboardinfo.todayCases = val.todayCases;
    dashboardinfo.todayDeaths = val.todayDeaths;
    dashboardinfo.todayRecovered = val.todayRecovered;
    return dashboardinfo;
  }

  ngOnDestroy() {
    this.ds_sub ? this.ds_sub.unsubscribe() : null;
  }
}
