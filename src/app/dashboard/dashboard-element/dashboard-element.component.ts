import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-element',
  templateUrl: './dashboard-element.component.html',
  styleUrls: ['./dashboard-element.component.css'],
})
export class DashboardElementComponent implements OnInit {
  @Input() title: string;
  @Input() count: number;
  constructor() {}

  ngOnInit(): void {}
}
